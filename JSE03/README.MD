# JAVA EE 8 APPLICATION 

## SOFTWARE

- Java JDK 1.8 (Oracle HotSpot)
- Maven 3

## TECHNOLOGIES
..

## BUILD APPLICATION WITH MAVEN
```bash
mvn clean install
```
## RUN APPLICATION

```bash
java -jar target/task-manager-1.0.jar
```

## DEVELOPER

> Rose Vlasova <nikarose@ya.ru>