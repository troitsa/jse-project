package ru.vlasova.iteco.taskmanager.service;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.repository.TaskRepository;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.util.List;

public class TaskService {

    private final static TaskService instance = new TaskService();

    private TaskRepository taskRepository = new TaskRepository();

    private ProjectService projectService = ProjectService.getInstance();

    public static TaskService getInstance() {
        return instance;
    }

    public void create(String name, String description, String dateStart, String dateFinish) {
        Task task = new Task(name, description, DateUtil.parseDateFromString(dateStart), DateUtil.parseDateFromString(dateFinish));
        if (task != null) {
            taskRepository.addTask(task);
        }
    }

    public List<Task> getTasks(int projectIndex) {
        List<Project> projectList = projectService.getProjects();
        String projectId = projectList.get(projectIndex).getId();
        List<Task> tasks = taskRepository.getTasksByProject(projectId);
        return tasks;
    }

    public List<Task> getTasks() {
        List<Task> tasks = taskRepository.getTasks();
        return tasks;
    }

    public void remove(Task task) {
        taskRepository.remove(task);
    }

    public void remove(int id) {
        Task task = getTask(id);
        remove(task);
    }

    public Task getTask(int index) {
        return taskRepository.getTaskByIndex(index);
    }

    public void removeTasksByProject(Project project) {
        List<Task> taskList = taskRepository.getTasksByProject(project.getId());

        for (Task task : taskList) {
            taskRepository.remove(task);
        }
    }

    public void clearTasks() {
        taskRepository.clear();
    }

    public int countTask() {
        return taskRepository.getTasks().size();
    }

    public int countTaskByProject(Project project) {
        return taskRepository.getTasksByProject(project.getId()).size();
    }

}
