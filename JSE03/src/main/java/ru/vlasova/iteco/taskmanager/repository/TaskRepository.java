package ru.vlasova.iteco.taskmanager.repository;

import ru.vlasova.iteco.taskmanager.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository {

    private List<Task> tasks = new ArrayList<>();

    public List<Task> getTasks() {
        return tasks;
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

    public Task getTaskByIndex(int index) {
        return tasks.get(index);
    }

    public List<Task> getTasksByProject(String projectId) {
        List<Task> listTasks = new ArrayList<>();
        for(Task task : tasks) {
            if (task.getProjectId().equals(projectId)) {
                listTasks.add(task);
            }
        }
        return listTasks;
    }

    public void remove(Task task) {
        tasks.remove(task);
    }

    public void clear() {
        tasks.clear();
    }

}
