package ru.vlasova.iteco.taskmanager;

import ru.vlasova.iteco.taskmanager.control.TerminalController;

public class App {

    public static void main(String[] args) {
        TerminalController terminalController = new TerminalController();
        terminalController.runConsole();
    }

}
