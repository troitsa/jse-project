package ru.vlasova.iteco.taskmanager.service;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.repository.ProjectRepository;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.util.List;

public class ProjectService {

    private final static ProjectService instance = new ProjectService();

    ProjectRepository projectRepository = new ProjectRepository();

    TaskService taskService = TaskService.getInstance();

    public static ProjectService getInstance() {
        return instance;
    }

    public void create(String name, String description, String dateStart, String dateFinish) {
        Project project = new Project(name, description, DateUtil.parseDateFromString(dateStart), DateUtil.parseDateFromString(dateFinish));
        if (project != null) {
            projectRepository.addProject(project);
        }
    }

    public List<Project> getProjects() {
        List<Project> projects = projectRepository.getProjects();
        return projects;
    }

    public void remove(int index) {
        Project project = projectRepository.getProjectByIndex(index);
        projectRepository.remove(project);
        taskService.removeTasksByProject(project);
    }

    public Project getProject(int index) {
        return projectRepository.getProjectByIndex(index);
    }

    public void clearProjects() {
        projectRepository.clear();
    }

}
