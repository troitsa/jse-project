package ru.vlasova.iteco.taskmanager.control;

import ru.vlasova.iteco.taskmanager.enumeration.TerminalCommand;
import ru.vlasova.iteco.taskmanager.view.ConsoleView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TerminalController {

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private final ConsoleView consoleView = new ConsoleView();

    public void runConsole() {
        System.out.println("==== WELCOME TO TASK MANAGER ====");
        while (true) {
            TerminalCommand terminalCommand = TerminalCommand.EMPTY;
            String input = "";
            try {
                input = reader.readLine();
                terminalCommand = TerminalCommand.valueOf(input.toUpperCase());
                if (TerminalCommand.EXIT.equals(terminalCommand)) {
                    reader.close();
                    System.exit(1);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                doCommand(terminalCommand);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void doCommand(TerminalCommand terminalCommand) throws IOException {
        switch (terminalCommand) {
            case PROJECT_CLEAR:
                consoleView.clearProject();
                break;
            case PROJECT_CREATE:
                consoleView.createProject(reader);
                break;
            case PROJECT_LIST:
                consoleView.printProjects();
                break;
            case PROJECT_EDIT:
                consoleView.editProject(reader);
                break;
            case PROJECT_REMOVE:
                consoleView.removeProject(reader);
                break;
            case TASKS_CLEAR:
                consoleView.clearTask();
                break;
            case TASK_CREATE:
                consoleView.createTask(reader);
                break;
            case TASK_LIST:
                consoleView.printTasks(reader);
                break;
            case TASK_EDIT:
                consoleView.editTask(reader);
                break;
            case TASK_REMOVE:
                consoleView.removeTask(reader);
                break;
            case HELP:
                consoleView.help();
                break;
        }
    }

}
