package ru.vlasova.iteco.taskmanager.repository;

import ru.vlasova.iteco.taskmanager.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {

    private List<Project> projects = new ArrayList<>();

    public List<Project> getProjects() {
        return projects;
    }

    public void addProject(Project project) {
        projects.add(project);
    }

    public Project getProject(String id) {
        for (Project project : projects) {
            if (project.getId().equals(id)) {
                return project;
            }
        }
        return null;
    }

    public void remove(Project project) {
        projects.remove(project);
    }

    public Project getProjectByIndex(int index) {
        return projects.get(index);
    }

    public void clear() {
        projects.clear();
    }

}
