package ru.vlasova.iteco.taskmanager.util;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    private final static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    public static Date parseDateFromString(String stringDate) {
        return dateFormatter.parse(stringDate, new ParsePosition(0));
    }

    public static String parseDateToString(Date date) {
        return dateFormatter.format(date);
    }

}