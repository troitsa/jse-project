package ru.vlasova.iteco.taskmanager;

import ru.vlasova.iteco.taskmanager.commander.Commander;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("==== WELCOME TO TASK MANAGER ====");
        Commander commander = new Commander(reader);
        while(true) {
            String input = reader.readLine();
            if("exit".equals(input.toLowerCase())) {
                reader.close();
                System.exit(1);
            }
            commander.appDo(input);
        }
    }
}
