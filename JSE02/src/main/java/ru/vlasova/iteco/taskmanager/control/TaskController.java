package ru.vlasova.iteco.taskmanager.control;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public class TaskController {
    Project project;

    public TaskController(Project project) {
        this.project = project;
    }

    public List<Task> getTasks() {
        return project.getTasks();
    }

    public void addTask(BufferedReader reader) throws IOException {
        System.out.println("Creating task. Set name: ");
        Task task = new Task(reader.readLine());
        project.addTask(task);
        System.out.println("Task created.");
    }

    public void clearTasks() {
        project.clearTasks();
    }

    public void printTasks() {
        List<Task> tasks = getTasks();
        if(tasks.size()==0) {
            System.out.println("There are no tasks.");
        } else {
            int i = 1;
            for(Task task : tasks) {
                System.out.println(i + ": " + task);
                i++;
            }
        }
    }

    public void removeTask(BufferedReader reader) throws IOException {
        System.out.println("Choose the task and type the number");
        printTasks();
        int num = Integer.parseInt(reader.readLine())-1;
        project.removeTask(getTasks().get(num));
        System.out.println("Task delete.");
    }

    public void editTask(BufferedReader reader) throws IOException {
        System.out.println("Choose the task and type the number");
        printTasks();
        int num = Integer.parseInt(reader.readLine())-1;
        Task task = getTasks().get(num);
        System.out.println("Editing task: " + task.getName() + ". Set new name: ");
        task.setName(reader.readLine());
        System.out.println("Task edit.");
    }
}
