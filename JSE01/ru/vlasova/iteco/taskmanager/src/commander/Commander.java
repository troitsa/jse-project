package commander;

import control.ProjectControler;
import java.io.BufferedReader;
import java.io.IOException;

public class Commander {
    private BufferedReader br;
    private final ProjectControler projectControler = new ProjectControler();

    public Commander(BufferedReader br) {
        this.br = br;
    }

    public void appDo(String command) throws IOException {
        switch (command.toLowerCase()) {
            case "project_clear":
                projectControler.clear();
                break;
            case "project_create":
                projectControler.create(br);
                break;
            case "project_list":
                projectControler.printProjects();
                break;
            case "project_edit":
                projectControler.edit(br);
                break;
            case "project_remove":
                projectControler.remove(br);
                break;
            case "task_clear":
                break;
            case "task_create":
                projectControler.addTask(br);
                break;
            case "task_list":
                projectControler.printTasks(br);
                break;
            case "task_edit":
                projectControler.editTask(br);
                break;
            case "task_remove":
                projectControler.removeTask(br);
                break;
            case "tasks_clear":
                projectControler.clearTasks(br);
                break;
            case "help":
                help();
                break;
        }
    }

    private void help() {
        System.out.println(" =====HELP=====\n"+
                "project_clear: Remove all projects.\n" +
                "project_create: Create new project.\n" +
                "project_list: Show all projects.\n" +
                "project_edit: Edit selected project\n" +
                "project_remove: Remove selected project\n" +
                "task_clear: Remove all tasks.\n" +
                "task_create: Create new task.\n" +
                "task_list: Show all tasks in project.\n" +
                "task_edit: Edit selected task.\n" +
                "task_remove: Remove selected task.\n" +
                "tasks_clear: clear tasks.\n" +
                "======================");
    }
}
