package control;

import entity.Project;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class ProjectControler {

    private final List<Project> projects = new ArrayList<>();

    public void clear() {
        projects.clear();
    }

    public void create(BufferedReader reader) throws IOException {
        System.out.println("Creating project. Set name: ");
        projects.add(new Project(reader.readLine()));
        System.out.println("Project created.");
    }

    public void printProjects() {
        for(int i = 0; i < projects.size(); i++) {
            System.out.println(i+1 + ": " + projects.get(i).getName());
        }
    }

    public void edit(BufferedReader reader) throws IOException {
        System.out.println("To edit project choose the project and type the number");
        printProjects();
        int num = Integer.parseInt(reader.readLine())-1;
        System.out.println("Editing project: " + projects.get(num).getName() + ". Set new name: ");
        projects.get(num).setName(reader.readLine());
        System.out.println("Project edit.");
    }

    public void remove(BufferedReader reader) throws IOException {
        System.out.println("To delete project choose the project and type the number");
        printProjects();
        int num = Integer.parseInt(reader.readLine())-1;
        projects.remove(num);
        System.out.println("Project delete.");
    }

    public void addTask(BufferedReader reader) throws IOException {
        getTaskController(reader).addTask(reader);
    }

    public void printTasks(BufferedReader reader) throws IOException {
        getTaskController(reader).printTasks();
    }

    public void clearTasks(BufferedReader reader) throws IOException {
        getTaskController(reader).clearTasks();
    }

    public void editTask(BufferedReader reader) throws IOException {
        getTaskController(reader).editTask(reader);
    }

    public void removeTask(BufferedReader reader) throws IOException {
        getTaskController(reader).removeTask(reader);
    }

    private TaskController getTaskController(BufferedReader reader) throws IOException {
        int projectId = -1;
        do {
            System.out.println("Please, choose the project and type the number");
            printProjects();
            projectId = Integer.parseInt(reader.readLine())-1;
        }
        while(projectId < 0 || projectId >= projects.size());
        TaskController taskController = new TaskController(projects.get(projectId));
        return taskController;
    }
}
